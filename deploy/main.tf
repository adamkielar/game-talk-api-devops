terraform {
  backend "s3" {
    bucket         = "game-talk-api-devops-tfstate"
    key            = "game-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "game-talk-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.68.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManageBy    = "Terraform"
  }
}

data "aws_region" "current" {}
