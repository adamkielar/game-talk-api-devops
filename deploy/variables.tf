variable "prefix" {
  type        = string
  default     = "game"
  description = "Recipe"
}

variable "project" {
  default = "game-talk-api-devops"
}

variable "contact" {
  default = "adam@adamkielar.pl"
}

variable "db_username" {
  description = "Username for RDS postgres instance"
}

variable "db_password" {
  description = "Password for RDS postgres instance"
}

variable "bastion_key_name" {
  default = "game-talk-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "623248211836.dkr.ecr.us-east-1.amazonaws.com/game-talk-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "623248211836.dkr.ecr.us-east-1.amazonaws.com/game-talk-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "gametalk.info"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}
