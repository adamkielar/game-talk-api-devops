Django>=3.0.7,<3.1.0
djangorestframework>=3.11.0,<3.12.0
djoser>=2.0.3,<2.1.0
django-filter>=2.3.0,<2.4.0
django-trench>=0.2.3,<0.3.0
drf-yasg>=1.17.1,<1.18.0
drf-extensions>=0.6.0,<0.7.0
psycopg2>=2.8.5,<2.9.0
Pillow>=7.1.2,<7.2.0
uwsgi>=2.0.19,<2.1.0
boto3>=1.14.6,<1.15.0
django-storages>=1.9.1,<1.10.0
coverage>=5.1,<5.2
flake8>=3.8.3,<3.9.0


