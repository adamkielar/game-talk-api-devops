from unittest.mock import patch

from django.test import TestCase

from core.core_extras import sample_user
from player.player_extras import avatar_image_file_path
from player.models import Player


class PlayerModelTests(TestCase):

    def test_player_str(self):
        """
        Test the player string representation.
        """
        player = Player.objects.create(
            owner=sample_user(),
            name='Kenshin',
        )

        self.assertEqual(str(player), player.name)

    @patch('uuid.uuid4')
    def test_avatar_file_name_uuid(self, mock_uuid):
        """
        Test that image is saved in the correct location.
        """
        uuid = 'test-uuid'
        mock_uuid.return_value = uuid
        filepath = avatar_image_file_path(None, 'my_avatar.jpg')
        exp_path = f'uploads/avatar/{uuid}.jpg'

        self.assertEqual(filepath, exp_path)
