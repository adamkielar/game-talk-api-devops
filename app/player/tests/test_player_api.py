import tempfile
import os

from PIL import Image

from django.contrib.auth import get_user_model
from django.urls import reverse
from django.test import TestCase

from rest_framework import status
from rest_framework.test import APIClient

from player.models import Player
from player.serializers import PlayerSerializer

PLAYERS_URL = reverse('player:player-list')


def avatar_upload_url(player_id):
    """
    Return URL for player avatar upload.
    """
    return reverse('player:player-upload-avatar', args=[player_id])


def detail_url(player_id):
    """
    Return URL for player details.
    """
    return reverse('player:player-detail', args=[player_id])


class PublicPlayersApiTests(TestCase):
    """
    Test the publicly available player API.
    """

    def setUp(self):
        self.client = APIClient()

    def test_login_required(self):
        """
        Test that login is required to access endpoint.
        """
        response = self.client.get(PLAYERS_URL)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class PrivatePlayerApiTests(TestCase):
    """
    Test the privately available player API.
    """

    def setUp(self):
        self.client = APIClient()
        self.user = get_user_model().objects.create_user(
            'test@adamkielar',
            'testuser',
            'testpass'
        )
        self.client.force_authenticate(self.user)

    def test_retrieve_player_list(self):
        """
        Test retrieving a list of players.
        """
        Player.objects.create(owner=self.user, name='Kenshin')
        Player.objects.create(owner=self.user, name='Battosai')

        response = self.client.get(PLAYERS_URL)
        players = Player.objects.all().order_by('-name')
        serializer = PlayerSerializer(players, many=True)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)

    def test_players_limited_to_user(self):
        """
        Test retrieving players only for owners.
        """
        user2 = get_user_model().objects.create_user(
            'test2@adamkielar.pl',
            'seconduser',
            'testpass'
        )
        Player.objects.create(owner=user2, name='Kenshin')
        Player.objects.create(owner=self.user, name='Battosai')

        response = self.client.get(PLAYERS_URL)
        players = Player.objects.filter(owner=self.user)
        serializer = PlayerSerializer(players, many=True)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data, serializer.data)

    def test_view_player_detail(self):
        """
        Test viewing player detail.
        """
        player = Player.objects.create(owner=self.user, name='Kenshin')
        url = detail_url(player.id)
        response = self.client.get(url)
        serializer = PlayerSerializer(player)

        self.assertEqual(response.data, serializer.data)

    def test_create_player_success(self):
        """
        Test creating player.
        """
        payload = {'name': 'Soulblighter'}
        response = self.client.post(PLAYERS_URL, payload)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        exists = Player.objects.filter(owner=self.user,
                                       name=payload['name']).exists()

        self.assertTrue(exists)

    def test_create_player_invalid(self):
        """
        Test creating invalid player fails.
        """
        payload = {'name': ''}
        response = self.client.post(PLAYERS_URL, payload)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_player(self):
        """
        Test update player with patch.
        """
        player = Player.objects.create(owner=self.user, name='Kenshin')
        payload = {'bio': 'Fight'}
        url = detail_url(player.id)
        self.client.patch(url, payload)

        player.refresh_from_db()
        self.assertEqual(player.name, 'Kenshin')
        self.assertEqual(player.bio, payload['bio'])


class PlayerAvatarUploadTests(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.user = get_user_model().objects.create_user(
            'test@adamkielar',
            'testuser',
            'testpass'
        )
        self.client.force_authenticate(self.user)
        self.player = Player.objects.create(owner=self.user, name='Kenshin')

    def tearDown(self):
        self.player.avatar.delete()

    def test_upload_avatar_to_player(self):
        """
        Test uploading avatar to player.
        """
        url = avatar_upload_url(self.player.id)
        with tempfile.NamedTemporaryFile(suffix='.jpg') as ntf:
            img = Image.new('RGB', (10, 10))
            img.save(ntf, format='JPEG')
            ntf.seek(0)
            response = self.client.post(url, {'avatar': ntf},
                                        format='multipart')

            self.player.refresh_from_db()
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertIn('avatar', response.data)
            self.assertTrue(os.path.exists(self.player.avatar.path))

    def test_upload_avatar_bad_request(self):
        """
        Test uploading an invalid image.
        """
        url = avatar_upload_url(self.player.id)
        response = self.client.post(url, {'avatar': 'nofile'},
                                    format='multipart')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
