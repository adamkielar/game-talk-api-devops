import os
import uuid


def avatar_image_file_path(instance, filename):
    """
    Generate file path for new avatar image.
    """
    ext = filename.split('.')[-1]
    filename = f'{uuid.uuid4()}.{ext}'

    return os.path.join('uploads/avatar/', filename)
