from django.db import models
from django.conf import settings

from player.player_extras import avatar_image_file_path


class Player(models.Model):
    """
    Player object.
    """
    owner = models.ForeignKey(settings.AUTH_USER_MODEL,
                              on_delete=models.CASCADE)
    name = models.CharField(max_length=40, unique=True)
    bio = models.CharField(max_length=255, blank=True)
    avatar = models.ImageField(null=True, upload_to=avatar_image_file_path)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-name']
