from django.urls import path, include
from rest_framework.routers import DefaultRouter

from player.views import PlayerViewSet


router = DefaultRouter()
router.register('players', PlayerViewSet)

app_name = 'player'

urlpatterns = [
    path('', include(router.urls))
]
