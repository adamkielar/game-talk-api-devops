from rest_framework import serializers

from player.models import Player


class PlayerSerializer(serializers.ModelSerializer):
    """
    Serializer for Player objects.
    """

    class Meta:
        model = Player
        fields = ('id', 'name', 'bio')
        read_only_fields = ('id',)


class PlayerImageSerializer(serializers.ModelSerializer):
    """
    Serializer for uploading avatars to players.
    """

    class Meta:
        model = Player
        fields = ('id', 'avatar')
        read_only_fields = ('id',)
