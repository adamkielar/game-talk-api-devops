from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets, status

from player.models import Player
from player.serializers import PlayerSerializer, PlayerImageSerializer
from player.permissions import IsOwner


class PlayerViewSet(viewsets.ModelViewSet):
    """
    Manage players.
    """
    queryset = Player.objects.all()
    serializer_class = PlayerSerializer
    permission_classes = [IsOwner, IsAuthenticated]

    def get_queryset(self):
        return self.queryset.filter(owner=self.request.user)

    def get_serializer_class(self):
        """
        Return needed serializer class.
        """
        if self.action == 'retrieve':
            return PlayerSerializer
        elif self.action == 'upload_avatar':
            return PlayerImageSerializer

        return self.serializer_class

    def perform_create(self, serializer):
        """
        Create a new player.
        """
        serializer.save(owner=self.request.user)
        return super().perform_create(serializer)

    @action(methods=['POST'], detail=True, url_path='upload-avatar')
    def upload_avatar(self, request, pk=None):
        """
        Upload an avatar to player.
        """
        player = self.get_object()
        serializer = self.get_serializer(player, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
