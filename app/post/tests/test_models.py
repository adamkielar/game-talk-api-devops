from django.contrib.auth import get_user_model
from django.test import TestCase

from clan.models import Clan
from player.models import Player
from post.models import Post, Tag


class PostModelTest(TestCase):
    """
    Test for post app models.
    """

    def setUp(self):
        self.user = get_user_model().objects.create_user(
            'test@adamkielar',
            'testuser',
            'testpass'
        )
        self.player = Player.objects.create(
            owner=self.user,
            name='Kenshin',
        )
        self.clan = Clan.objects.create(
            title='Holy Warriors',
            description='The Best',
            owner=self.user
        )

    def test_tag_str(self):
        """
        Test tag string representation.
        """
        tag = Tag.objects.create(
            name='War',
            owner=self.user
        )

        self.assertEqual(str(tag), tag.name)

    def test_post_str(self):
        """
        Test post string representation.
        """
        post = Post.objects.create(
            owner=self.user,
            message='Defend the castle',
            clan=self.clan
        )

        self.assertEqual(str(post), post.message)
