from django.contrib.auth import get_user_model
from django.urls import reverse
from django.test import TestCase

from rest_framework import status
from rest_framework.test import APIClient

from clan.models import Clan
from player.models import Player
from post.models import Tag, Post
from post.serializers import TagSerializer

TAGS_URL = reverse('post:tag-list')


class PublicTagsApiTests(TestCase):
    """
    Test the publicly available tag API.
    """

    def setUp(self):
        self.client = APIClient()

    def test_login_required(self):
        """
        Test that login is required to retrieve tags.
        """
        response = self.client.get(TAGS_URL)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class PrivateTagsApiTest(TestCase):
    """
    Test the authorized player tags API.
    """

    def setUp(self):
        self.client = APIClient()
        self.user = get_user_model().objects.create_user(
            'test@adamkielar',
            'testuser',
            'testpass'
        )
        self.client.force_authenticate(self.user)
        self.player = Player.objects.create(owner=self.user, name='Kenshin')
        self.clan = Clan.objects.create(owner=self.user, title='Holy Warriors')

    def test_retrieve_tags(self):
        """
        Test retrieving tags.
        """
        Tag.objects.create(owner=self.user, name='Peace')
        Tag.objects.create(owner=self.user, name='War')

        response = self.client.get(TAGS_URL)
        tags = Tag.objects.all()
        serializer = TagSerializer(tags, many=True)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)

    def test_tag_limited_to_user(self):
        """
        Test that returned tags are for correct user.
        """
        user2 = get_user_model().objects.create_user(
            'test2@adamkielar.pl',
            'seconduser',
            'testpass'
        )
        Tag.objects.create(owner=user2, name='Attack')
        tag = Tag.objects.create(owner=self.user, name='Defend')
        response = self.client.get(TAGS_URL)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['name'], tag.name)

    def test_create_tag_success(self):
        """
        Test creating a new tag.
        """
        payload = {'name': 'Test tag'}
        self.client.post(TAGS_URL, payload)
        exists = Tag.objects.filter(owner=self.user,
                                    name=payload['name']).exists()

        self.assertTrue(exists)

    def test_create_tag_invalid(self):
        """
        Test creating invalid tag fails.
        """
        payload = {'name': ''}
        response = self.client.post(TAGS_URL, payload)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_retrieve_tags_assigned_to_post(self):
        """
        Test filtering tags by those assigned to posts.
        """
        tag1 = Tag.objects.create(owner=self.user, name='Hide')
        tag2 = Tag.objects.create(owner=self.user, name='Run')
        post = Post.objects.create(
            owner=self.user,
            message='We need to hide now.',
            clan=self.clan
        )
        post.tags.add(tag1)

        response = self.client.get(TAGS_URL, {'assigned_only': 1})
        serializer1 = TagSerializer(tag1)
        serializer2 = TagSerializer(tag2)

        self.assertIn(serializer1.data, response.data)
        self.assertNotIn(serializer2.data, response.data)

    def test_retrieve_tags_assigned_unique(self):
        """
        Test filtering tags and check if returns unique posts.
        """
        tag = Tag.objects.create(owner=self.user, name='Hide')
        Tag.objects.create(owner=self.user, name='Run')
        post1 = Post.objects.create(
            owner=self.user,
            message='We need to hide now.',
            clan=self.clan,
        )
        post1.tags.add(tag)
        post2 = Post.objects.create(
            owner=self.user,
            message='We need to run now.',
            clan=self.clan,
        )
        post2.tags.add(tag)

        response = self.client.get(TAGS_URL, {'assigned_only': 1})

        self.assertEqual(len(response.data), 1)
