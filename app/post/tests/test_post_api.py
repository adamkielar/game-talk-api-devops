from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse

from rest_framework import status
from rest_framework.test import APIClient

from post.models import Post, Tag
from post.serializers import PostSerializer

POSTS_URL = reverse('post:post-list')


def detail_url(post_id):
    """
    Return post detail URL.
    """
    return reverse('post:post-detail', args=[post_id])


def sample_tag(owner, name='Attack'):
    """
    Create and return sample tag.
    """
    return Tag.objects.create(owner=owner, name=name)


def sample_post(owner, **params):
    """
    Create and return a sample post.
    """
    defaults = {
        'message': 'We need to gather our forces.'
    }
    defaults.update(params)
    return Post.objects.create(owner=owner, **defaults)


class PublicPostApiTests(TestCase):
    """
    Test unauthenticated post API access.
    """

    def setUp(self):
        self.client = APIClient()

    def test_auth_required(self):
        """
        Test that authentication is required.
        """
        response = self.client.get(POSTS_URL)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class PrivatePostApiTests(TestCase):
    """
    Test authenticated post API access.
    """

    def setUp(self):
        self.client = APIClient()
        self.user = get_user_model().objects.create_user(
            'test@adamkielar.pl',
            'testuser',
            'testpass'
        )
        self.client.force_authenticate(self.user)

    def test_retrieve_posts(self):
        """
        Test retrieving a list of posts.
        """
        sample_post(owner=self.user)
        sample_post(owner=self.user, message='We need to flee.')

        response = self.client.get(POSTS_URL)
        posts = Post.objects.all()
        serializer = PostSerializer(posts, many=True)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)

    def test_post_detail_view(self):
        """
        Test viewing post detail.
        """
        post = sample_post(owner=self.user)
        post.tags.add(sample_tag(owner=self.user))

        url = detail_url(post.id)
        response = self.client.get(url)
        serializer = PostSerializer(post)

        self.assertEqual(response.data, serializer.data)

    def test_create_post_success(self):
        """
        Test creating post.
        """
        payload = {'message': 'Meet me in the Unholy Cave.'}
        response = self.client.post(POSTS_URL, payload)
        exists = Post.objects.filter(owner=self.user,
                                     message=payload['message']).exists()

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(exists)

    def test_create_post_with_tags(self):
        """
        Test creating a post with tags.
        """
        tag1 = sample_tag(owner=self.user, name='Voodoo')
        tag2 = sample_tag(owner=self.user, name='Spell')
        payload = {
            'message': 'I found secret voodoo spell.',
            'tags': [tag1.id, tag2.id]
        }
        response = self.client.post(POSTS_URL, payload)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        post = Post.objects.get(id=response.data['id'])
        tags = post.tags.all()
        self.assertEqual(tags.count(), 2)
        self.assertIn(tag1, tags)
        self.assertIn(tag2, tags)

    def test_filter_posts_by_tags(self):
        """
        Test returning posts with specific tags.
        """
        post1 = sample_post(owner=self.user, message='In the Void')
        post2 = sample_post(owner=self.user, message='In the Abyss')
        tag1 = sample_tag(owner=self.user, name='Fly')
        tag2 = sample_tag(owner=self.user, name='Cast')
        post1.tags.add(tag1)
        post2.tags.add(tag2)
        post3 = sample_post(owner=self.user, message='In the Forest.')

        response = self.client.get(POSTS_URL, {'tags': f'{tag1.id},{tag2.id}'})
        serializer1 = PostSerializer(post1)
        serializer2 = PostSerializer(post2)
        serializer3 = PostSerializer(post3)

        self.assertIn(serializer1.data, response.data)
        self.assertIn(serializer2.data, response.data)
        self.assertNotIn(serializer3.data, response.data)
