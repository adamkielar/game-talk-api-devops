from rest_framework import mixins, viewsets
from rest_framework.permissions import IsAuthenticated

from post.models import Tag, Post
from post.serializers import TagSerializer, PostSerializer
from player.permissions import IsOwner


class TagViewSet(viewsets.GenericViewSet, mixins.ListModelMixin,
                 mixins.CreateModelMixin):
    """
    Manage tags.
    """
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    permission_classes = [IsOwner, IsAuthenticated]

    def get_queryset(self):
        """
        Return tags for current user.
        """
        assigned_only = bool(
            int(self.request.query_params.get('assigned_only', 0)))
        queryset = self.queryset
        if assigned_only:
            queryset = queryset.filter(post__isnull=False)

        return queryset.filter(owner=self.request.user).order_by(
            '-name').distinct()

    def perform_create(self, serializer):
        """
        Create a new tag.
        """
        serializer.save(owner=self.request.user)
        return super().perform_create(serializer)


class PostViewSet(viewsets.ModelViewSet):
    """
    Manage posts.
    """
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [IsOwner, IsAuthenticated]

    def _params_to_inits(self, qs):
        """
        Convert a list of string IDs to a list of integers.
        """
        return [int(str_id) for str_id in qs.split(',')]

    def get_queryset(self):
        """
        Retrieve post for auth user.
        """
        tags = self.request.query_params.get('tags')
        queryset = self.queryset
        if tags:
            tag_ids = self._params_to_inits(tags)
            queryset = queryset.filter(tags__id__in=tag_ids).prefetch_related(
                'tags')

        return queryset

    def perform_create(self, serializer):
        """
        Create a new post.
        """
        serializer.save(owner=self.request.user)
