from rest_framework import serializers

from post.models import Tag, Post


class TagSerializer(serializers.ModelSerializer):
    """
    Serializer for tag objects.
    """

    class Meta:
        model = Tag
        fields = ('id', 'name',)
        read_only_fields = ('id',)


class PostSerializer(serializers.ModelSerializer):
    """
    Serializer for Post objects.
    """
    tags = serializers.PrimaryKeyRelatedField(many=True,
                                              queryset=Tag.objects.all())

    class Meta:
        model = Post
        fields = ('id', 'message', 'clan', 'tags', 'created')
