from django.db import models
from django.conf import settings

from clan.models import Clan


class Tag(models.Model):
    """
    Tag to be used for post.
    """
    name = models.CharField(max_length=255)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL,
                              on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-name']


class Post(models.Model):
    """
    Model for Post objects.
    """
    owner = models.ForeignKey(settings.AUTH_USER_MODEL,
                              on_delete=models.CASCADE)
    message = models.TextField()
    clan = models.ForeignKey(Clan, on_delete=models.CASCADE, null=True,
                             blank=True)
    tags = models.ManyToManyField(Tag)
    created = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.message

    class Meta:
        ordering = ['-created']
        unique_together = ['owner', 'message']
