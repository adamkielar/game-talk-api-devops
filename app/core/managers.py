from django.contrib.auth.models import BaseUserManager
from django.utils.translation import ugettext_lazy as _


class UserManager(BaseUserManager):
    """
    Custom user model manager.
    """

    def create_user(self, email, username, display_name=None, password=None,
                    **extra_fields):
        """
        Create and save a user.
        """
        if not email:
            raise ValueError(_("User must have an email address"))
        if not display_name:
            display_name = username
        user = self.model(
            email=self.normalize_email(email),
            username=username,
            display_name=display_name,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, display_name, password,
                         **extra_fields):
        """
        Create and save a superuser.
        """
        user = self.create_user(email, username, display_name, password,
                                **extra_fields)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user
