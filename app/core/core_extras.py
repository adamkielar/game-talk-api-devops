from django.contrib.auth import get_user_model


def sample_user(email='test@adamkielar.pl', username='testuser',
                password='testpass'):
    """
    Create s sample user
    """
    return get_user_model().objects.create_user(email, username, password)
