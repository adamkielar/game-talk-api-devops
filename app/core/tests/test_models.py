from django.test import TestCase
from django.contrib.auth import get_user_model


class ModelTests(TestCase):
    def test_create_user_with_email_success(self):
        """
        Test creating a new user with an email - success.
        """
        email = 'test@adamkielar.pl'
        username = 'testuser'
        password = 'testpass'
        user = get_user_model().objects.create_user(email=email,
                                                    username=username,
                                                    password=password)

        self.assertEqual(user.email, email)
        self.assertEqual(user.username, username)
        self.assertTrue(user.check_password(password))

    def test_new_user_email_normalized(self):
        """
        Test that email for a new user is normalized.
        """
        email = 'test@ADAMKIELAR.pl'
        user = get_user_model().objects.create_user(email, 'testpass')

        self.assertEqual(user.email, email.lower())

    def test_new_user_invalid_email(self):
        """
        Test creating new user with no email.
        """
        with self.assertRaises(ValueError):
            get_user_model().objects.create_user(None, 'testpass')

    def test_create_new_superuser(self):
        """
        Test creating a new superuser.
        """
        email = 'test@adamkielar.pl'
        username = 'testuser'
        display_name = 'Test User'
        password = 'testpass'
        user = get_user_model().objects.create_superuser(
            email=email,
            username=username,
            display_name=display_name,
            password=password
        )

        self.assertTrue(user.is_superuser)
        self.assertTrue(user.is_staff)
