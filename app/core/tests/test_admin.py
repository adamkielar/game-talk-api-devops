from django.test import TestCase, Client
from django.contrib.auth import get_user_model
from django.urls import reverse


class AdminSiteTests(TestCase):

    def setUp(self):
        self.client = Client()
        self.admin_user = get_user_model().objects.create_superuser(
            email='admin@adamkielar.pl',
            username='testadmin',
            display_name='Test Admin',
            password='testpassword'
        )
        self.client.force_login(self.admin_user)
        self.user = get_user_model().objects.create_user(
            email='adam@adamkielar.pl',
            username='testusername',
            display_name='Adam Kielar',
            password='testpassword',
        )

    def test_users_listed(self):
        """
        Test that users are listed on user page.
        """
        url = reverse('admin:core_user_changelist')
        res = self.client.get(url)

        self.assertContains(res, self.user.display_name)
        self.assertContains(res, self.user.email)

    def test_user_change_page(self):
        """
        Test the user edit page works.
        """
        url = reverse('admin:core_user_change', args=[self.user.id])
        res = self.client.get(url)

        self.assertEqual(res.status_code, 200)

    def test_create_user_page(self):
        """
        Test the create user page works.
        """
        url = reverse('admin:core_user_add')
        res = self.client.get(url)

        self.assertEqual(res.status_code, 200)
