from rest_framework_extensions.mixins import NestedViewSetMixin
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from clan.filters import MembershipFilterSet
from clan.models import Clan, ClanMember
from clan.serializers import ClanCreateSerializer, ClanUpdateSerializer, \
    ClanMemberJoinSerializer, \
    ClanMemberUpdateSerializer
from clan.permissions import IsOwner, IsMember


class ClanViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    """
    Manage clans.
    """
    queryset = Clan.objects.all().prefetch_related('members')
    serializer_class = ClanUpdateSerializer
    permission_classes = [IsOwner, IsAuthenticated]

    def get_permissions(self):
        """
        Return the list of permissions that this view requires.
        """
        if self.action in ['update', 'partial_update', 'destroy']:
            permission_classes = [IsOwner]
        else:
            permission_classes = [IsAuthenticated]
        return [permission() for permission in permission_classes]

    def get_serializer_class(self):
        """
        Return appropriate serializer class.
        """
        if self.action == 'create':
            return ClanCreateSerializer
        if self.action == 'join_clan':
            return ClanMemberJoinSerializer
        return self.serializer_class

    @action(methods=['POST'], detail=True, url_path='join-clan',
            permission_classes=[IsAuthenticated])
    def join_clan(self, request, pk=None):
        user = self.request.user
        clan = self.get_object()
        ClanMember.objects.create(clan=clan, user=user, role=1)
        return Response({'message': f'You are now a member of {clan} clan.'})


class ClanMemberViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    queryset = ClanMember.objects.all().select_related('clan', 'user')
    serializer_class = ClanMemberUpdateSerializer
    permission_classes = [IsMember, IsAuthenticated]
    allowed_methods = ['GET', 'DELETE', 'HEAD', 'OPTIONS']
    filterset_class = MembershipFilterSet

    def get_permissions(self):
        """
        Return the list of permissions that this view requires.
        """
        if self.action == 'destroy':
            permission_classes = [IsMember]
        else:
            permission_classes = [IsAuthenticated]
        return [permission() for permission in permission_classes]
