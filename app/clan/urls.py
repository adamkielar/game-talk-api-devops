from rest_framework_extensions.routers import ExtendedSimpleRouter

from clan.views import ClanViewSet, ClanMemberViewSet

router = ExtendedSimpleRouter()
(
    router.register('clans', ClanViewSet, basename='clan')
        .register('members',
                  ClanMemberViewSet,
                  basename='clans-members',
                  parents_query_lookups=['clan_id'])
)

app_name = 'clan'

urlpatterns = router.urls
