from django.db import models
from django.conf import settings

MEMBERSHIP_CHOICES = (
    (0, 'banned'),
    (1, 'member'),
    (2, 'moderator'),
    (3, 'admin')
)


class Clan(models.Model):
    """
    Model for Clan object.
    """
    owner = models.ForeignKey(settings.AUTH_USER_MODEL,
                              on_delete=models.CASCADE)
    title = models.CharField(max_length=255, unique=True)
    description = models.CharField(max_length=255, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    members = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                     related_name='clan_members',
                                     through='ClanMember')

    def __str__(self):
        return self.title

    @property
    def admin(self):
        return self.clan.filter(role=3).value_list('user', flat=True)

    @property
    def moderators(self):
        return self.clan.filter(role=2).value_list('user', flat=True)

    @property
    def good_members(self):
        return self.clan.exclude(role=0)

    class Meta:
        ordering = ['-title']


class ClanMember(models.Model):
    """
    Model for Clan Member object.
    """
    clan = models.ForeignKey(Clan, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='member',
                             on_delete=models.CASCADE)
    role = models.SmallIntegerField(choices=MEMBERSHIP_CHOICES, default=1)
    joined = models.DateTimeField(auto_now_add=True)
    left = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.user.username

    class Meta:
        permissions = (
            ('ban_player', 'You cab ban players'),
        )
        unique_together = ('clan', 'user')
