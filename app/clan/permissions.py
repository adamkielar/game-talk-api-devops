from rest_framework import permissions


class IsOwner(permissions.BasePermission):
    """
    Only owners of an object can edit it.
    """
    message = "You are not the owner of this clan"

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return request.user == obj.owner


class IsMember(permissions.BasePermission):
    """
    Only owners of an object can edit it.
    """
    message = "You are not the owner of this clan"

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return request.user == obj.user
