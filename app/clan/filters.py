import django_filters

from clan.models import ClanMember


class MembershipFilterSet(django_filters.FilterSet):
    """
    Filter clan members by their role in clan.
    """

    class Meta:
        model = ClanMember
        fields = ('role',)
