from django.contrib.auth import get_user_model
from django.test import TestCase

from clan.models import Clan, ClanMember


class ClanModelsTest(TestCase):
    """
    Test clan app models.
    """

    def setUp(self):
        self.user = get_user_model().objects.create_user(
            'test@adamkielar',
            'testuser',
            'testpass'
        )
        self.clan = Clan.objects.create(
            title='Holy Warriors',
            description='The Best',
            owner=self.user
        )

    def test_clan_str(self):
        """
        Test clan string representation.
        """
        clan1 = self.clan

        self.assertEqual(str(clan1), clan1.title)

    def test_clan_member_str(self):
        """
        Test clan member string representation.
        """
        clan_member = ClanMember.objects.create(
            clan=self.clan,
            user=self.user,
        )

        self.assertEqual(str(clan_member), self.user.username)
