from django.contrib.auth import get_user_model
from django.urls import reverse
from django.test import TestCase

from rest_framework import status
from rest_framework.test import APIClient

from clan.models import Clan
from clan.serializers import ClanCreateSerializer, ClanUpdateSerializer

CLANS_URL = reverse('clan:clan-list')


def detail_url(clan_id):
    """
    Return URL for clan details.
    """
    return reverse('clan:clan-detail', args=[clan_id])


class PublicClanApiTests(TestCase):
    """
    Test the publicly available clan API.
    """

    def setUp(self):
        self.client = APIClient()

    def test_login_required(self):
        """
        Test that login is required to access endpoint.
        """
        response = self.client.get(CLANS_URL)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class PrivateClanApiTests(TestCase):
    """
    Test the privately available clan API.
    """

    def setUp(self):
        self.client = APIClient()
        self.user = get_user_model().objects.create_user(
            'test@adamkielar',
            'testuser',
            'testpass'
        )
        self.client.force_authenticate(self.user)
        self.clan = Clan.objects.create(owner=self.user, title='Holy Warriors')
        self.members = self.user

    def test_retrieve_clan_list(self):
        """
        Test retrieving a list of clans.
        """
        Clan.objects.create(owner=self.user, title='Sneaky Assasins')

        response = self.client.get(CLANS_URL)
        clans = Clan.objects.all().order_by('-title')
        serializer = ClanUpdateSerializer(clans, many=True)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)

    def test_view_clan_detail(self):
        """
        Test clan detail view.
        """
        clan = self.clan
        clan.members.add(self.members)
        url = detail_url(clan.id)
        response = self.client.get(url)
        serializer = ClanUpdateSerializer(clan)

        self.assertEqual(response.data, serializer.data)

    def test_create_clan_success(self):
        """
        Test creating clan.
        """
        payload = {'title': 'Avengers', 'description': 'Masters'}
        request = self.client.post(CLANS_URL, payload)
        ClanCreateSerializer(payload, context={'request': request}).data

        self.assertEqual(request.status_code, status.HTTP_201_CREATED)

        exists = Clan.objects.filter(owner=self.user,
                                     title=payload['title']).exists()

        self.assertTrue(exists)

    def test_create_clan_invalid(self):
        """
        Test creating invalid clan fails.
        """
        payload = {'title': ''}
        response = self.client.post(CLANS_URL, payload)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_clan(self):
        """
        Test update clan with patch.
        """
        clan = Clan.objects.create(owner=self.user, title='Secret Coven')
        payload = {'description': 'Six Feet Under'}
        url = detail_url(clan.id)
        self.client.patch(url, payload)

        clan.refresh_from_db()
        self.assertEqual(clan.title, 'Secret Coven')
        self.assertEqual(clan.description, payload['description'])
