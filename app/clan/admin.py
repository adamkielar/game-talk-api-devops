from django.contrib import admin

from clan.models import Clan, ClanMember

admin.site.register(Clan)
admin.site.register(ClanMember)
