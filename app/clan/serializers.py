from django.contrib.auth import get_user_model

from rest_framework import serializers

from clan.models import Clan, ClanMember


class ClanCreateSerializer(serializers.ModelSerializer):
    """
    Serializer for Clan objects.
    """
    owner = serializers.HiddenField(default=serializers.CurrentUserDefault())
    title = serializers.CharField()
    description = serializers.CharField()

    class Meta:
        model = Clan
        fields = ('id', 'title', 'description', 'owner')
        read_only_fields = ('id', 'owner')

    def create(self, validated_data):
        members_data = validated_data.pop('owner')
        users = get_user_model().objects.filter(username=members_data)
        clan = Clan.objects.create(owner=members_data, **validated_data)
        for user in users:
            ClanMember.objects.create(clan=clan, user=user, role=3)
            clan.members.add(user)
        return clan


class ClanMemberJoinSerializer(serializers.ModelSerializer):
    """
    Serializer for ClanMember objects.
    """

    class Meta:
        model = Clan
        fields = ('id',)
        read_only_fields = ('id',)


class ClanUpdateSerializer(serializers.ModelSerializer):
    """
    Serializer for retrieve and update.
    """
    members = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Clan
        fields = ('id', 'title', 'description', 'members')
        read_only_fields = ('id', 'members')


class ClanMemberUpdateSerializer(serializers.ModelSerializer):
    """
    Serializer to update ClanMember.
    """

    class Meta:
        model = ClanMember
        fields = ('id', 'clan', 'user', 'role')
        read_only_fields = ('id', 'clan', 'user')
