# Generated by Django 3.0.7 on 2020-06-29 18:01

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('clan', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='clanmember',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='member', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='clan',
            name='members',
            field=models.ManyToManyField(related_name='clan_members', through='clan.ClanMember', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='clan',
            name='owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterUniqueTogether(
            name='clanmember',
            unique_together={('clan', 'user')},
        ),
    ]
